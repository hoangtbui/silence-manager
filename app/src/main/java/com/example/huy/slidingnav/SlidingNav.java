package com.example.huy.slidingnav;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.gesture.Gesture;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


/**
 * A simple {@link Fragment} subclass.
 */
public class SlidingNav extends Fragment {

    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private boolean bSavedInstanceState;
    private boolean navPreOpen; //if user open nav before open nav or not
    private static final String PREF_NAME = "mypref";
    private static final String NAV_PRE_OPENED = "nav_preopened";
    private Toolbar fragToolbar;
    private LinearLayout navigationDrawer, meeting, location, classroom;

    public SlidingNav() {
        // Required empty public constructor

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyPreference.preferenceWriter(getActivity(), "OPEN_MODE", "0");
        navPreOpen = Boolean.valueOf(MyPreference.preferenceReader(getActivity(), NAV_PRE_OPENED, "false"));
        if (savedInstanceState != null) {
            bSavedInstanceState = true;
        }


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //inflate the layout
        final View view = inflater.inflate(R.layout.fragment_sliding_nav, container, false);
        navigationDrawer = (LinearLayout) view.findViewById(R.id.navigatioDrawer);
        location = (LinearLayout) view.findViewById(R.id.location_nav);
        meeting = (LinearLayout) view.findViewById(R.id.meeting_nav);
        classroom = (LinearLayout) view.findViewById(R.id.classroom_nav);
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(view.getContext(),SilentLocation.class));
            }
        });
        classroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(view.getContext(),SilentClassroom.class));
            }
        });
        meeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(view.getContext(),SilentMeeting.class));
            }
        });


        // Inflate the layout for this fragment
        return view;
    }


    //handle the slidng nav when opened or closed by user
    public void display(int fragID, DrawerLayout layout, Toolbar toolbar) {
        drawerLayout = layout;
        fragToolbar = toolbar;
        drawerLayout.setDrawerShadow(R.drawable.nav_shadow, Gravity.LEFT);
        drawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_close, R.string.drawer_open) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!navPreOpen) {
                    navPreOpen = true;
                    MyPreference.preferenceWriter(getActivity(), PREF_NAME, navPreOpen + "");
                }
            }


            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);


            }


            @Override
            //slidOffset = posion when slding nav moves
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);


            }
        };

        //open drawer if user never open it
        //if (!navPreOpen && !bSavedInstanceState) {
        //    drawerLayout.openDrawer(getActivity().findViewById(fragID));
        //}

        drawerLayout.setDrawerListener(drawerToggle);
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                //display icon that let user press to open nav
                drawerToggle.syncState();
            }
        });
    }










}

