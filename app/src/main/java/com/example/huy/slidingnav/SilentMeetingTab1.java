package com.example.huy.slidingnav;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Huy on 7/24/2015.
 */
public class SilentMeetingTab1 extends Fragment {
    private ImageView addContactButton;
    private LinearLayout calendarList;
    private ViewGroup contactListUI;
    private List<Integer> selectedIndex;
    private List<ContactInfo> contactList;
    private CharSequence[] nameListCharSequence;
    private TextView contactSummaryTextView;
    private MyDB db;
    private View mainView;
    private Spinner minSpinner;
    private FragmentActivity myContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.silent_meeting_tab1, container, false);

        contactList = new ArrayList<ContactInfo>();
        contactListUI = (ViewGroup) mainView.findViewById(R.id.selectedContactList);
        calendarList = (LinearLayout) mainView.findViewById(R.id.calendar_list);
        addContactButton = (ImageView) mainView.findViewById(R.id.addCourseButton);
        contactSummaryTextView = (TextView) mainView.findViewById(R.id.contact_summary);
        selectedIndex = new ArrayList<Integer>();
        db = new MyDB(mainView.getContext());
        minSpinner = (Spinner) mainView.findViewById(R.id.minSpinnerClassroom);
        db.createCalendarTable();
        db.createContactTable();
        //set up min spinner
        List<String> minute = new ArrayList<String>();
        for (int i = 0; i <= 59; i++) {
            minute.add(i + "");
        }
        ArrayAdapter<String> spinnerAdapater = new ArrayAdapter<String>(mainView.getContext(), R.layout.simple_row, R.id.rowTV, minute);
        minSpinner.setAdapter(spinnerAdapater);


        //set up contact listview
        setContactListView();
        contactSummarySetText(db.getAllContact().size());

        //get all selected calendar
        final List<CalendarInfo> availCal = queryCalendar(mainView.getContext());


        //display calendars
        for (int i = 0; i < availCal.size(); i++) {
            View calendarRowView = LayoutInflater.from(mainView.getContext()).inflate(R.layout.calendar_list_row, null);
            TextView title = (TextView) calendarRowView.findViewById(R.id.calendar_title);
            title.setText(availCal.get(i).title);

            View color = calendarRowView.findViewById(R.id.calendar_color);
            color.setBackgroundColor(Integer.parseInt(availCal.get(i).color));
            final long calID = (availCal.get(i).id);
            final String calName = availCal.get(i).title;
            CheckBox checkBox = (CheckBox) calendarRowView.findViewById(R.id.calendar_checkbox);

            final List<CalendarInfo> selectedCal = db.getAllCalendar();


            for (int a = 0; a < selectedCal.size(); a++) {
                Log.wtf("selectedCal " + selectedCal.size(), selectedCal.get(a).id + "");
                if (selectedCal.get(a).id == calID) {
                    checkBox.setChecked(true);
                    break;
                } else checkBox.setChecked(false);
            }


            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (((CheckBox) v).isChecked() == true) {
                        db.insertCalendar(String.valueOf(calID), calName);
                    } else {
                        db.deleteCalendar(String.valueOf(calID));
                    }
                    List<CalendarInfo> selectedCal = db.getAllCalendar();
                    for (int a = 0; a < selectedCal.size(); a++) {
                        Log.wtf("count", selectedCal.size() + "");
                        Log.wtf("selected id", selectedCal.get(a).id + "");
                    }
                    /*Fragment tab2 = new SilentMeetingTab2();
                    FragmentTransaction tr = getFragmentManager().beginTransaction();
                    tr.replace(R.id.tab2Fragment, tab2);
                    tr.commit();*/
                }
            });

            calendarList.addView(calendarRowView);
            if ((i + 1) < availCal.size()) {
                View divider = LayoutInflater.from(mainView.getContext()).inflate(R.layout.my_list_divider, null);
                calendarList.addView(divider);
            }
        }


        //read contacts
        List<String> nameList = new ArrayList<>();
        Cursor cursor = mainView.getContext().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        while (cursor.moveToNext()) {
            String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

            if ("1".equals(hasPhone) || Boolean.parseBoolean(hasPhone)) {
                // check if contact has phone #
                Cursor phones = mainView.getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);

                while (phones.moveToNext()) {
                    ContactInfo temp = new ContactInfo();
                    String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    int itype = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));

                    final boolean isMobile =
                            itype == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE ||
                                    itype == ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE;

                    // Do something here with 'phoneNumber' such as saving into
                    // the List or Array that will be used in your 'ListView'.
                    //Log.wtf(name,phoneNumber);
                    temp.id = contactId;
                    temp.name = name;
                    temp.number = phoneNumber;
                    nameList.add(name);
                    contactList.add(temp);
                }
                phones.close();
            }
        }
        nameListCharSequence = nameList.toArray(new CharSequence[nameList.size()]);

        //finish reading contact////////////


        //dialog with checkbox for contacts
        addContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder contactListDialog = new AlertDialog.Builder(mainView.getContext());
                contactListDialog.setTitle("Select emergency contacts");

                //store selected index from alert dialog
                contactListDialog.setMultiChoiceItems(nameListCharSequence, null, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int index, boolean isChecked) {
                        //get selected contacts
                        final List<ContactInfo> savedContact = db.getAllContact();
                        final List<String> savedId = new ArrayList<String>();
                        for (int a = 0; a < savedContact.size(); a++) {
                            savedId.add(savedContact.get(a).id);
                        }
                        if (isChecked) {//if contact is slected and not selected before
                            if (savedContact.size() >= 10) {
                                AlertDialog message;
                                AlertDialog.Builder builder = new AlertDialog.Builder(mainView.getContext());
                                builder.setMessage("The number of Emergency contacts is limited to 10").setPositiveButton("Accept", null);
                                message = builder.create();
                                ((AlertDialog) dialog).getListView().setItemChecked(index, false);
                                message.show();
                            } else if (!savedId.contains(contactList.get(index).id)) {
                                db.insertContact(contactList.get(index).id, contactList.get(index).name, contactList.get(index).number);
                            }
                        } else if (savedId.contains(contactList.get(index).id))
                            db.deleteContact(contactList.get(index).id);
                    }//end onclick
                }).setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        contactListUI.removeAllViews();
                        setContactListView();


                        contactSummarySetText(db.getAllContact().size());
                    }//end onclick
                });
                AlertDialog dialog = contactListDialog.create();
                dialog.show();

            }
        });
        return mainView;
    }


    @Override
    public void onAttach(Activity activity) {
        myContext = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    //read db and set up listview to display contacts
    private void setContactListView() {
        final List<ContactInfo> savedContact = db.getAllContact();
        Log.wtf("contact count", savedContact.size() + "");
        for (int i = 0; i < savedContact.size(); i++) {
            final ViewGroup row = (ViewGroup) LayoutInflater.from(mainView.getContext()).inflate(R.layout.contact_list_row, contactListUI, false);
            //ContactInfo contact = contactList.get(selectedIndex.get(i));

            TextView name = (TextView) row.findViewById(R.id.contact_name);
            name.setText(savedContact.get(i).name);
            ImageView delete = (ImageView) row.findViewById(R.id.contact_delete);
            // TextView id = (TextView) row.findViewById(R.id.tv_contactId);
            //id.setText(savedContact.get(i).id);

            if ((i + 1) >= savedContact.size()) {
                View divider = row.findViewById(R.id.contact_divider);
                divider.setVisibility(View.INVISIBLE);
            }

            final String contactID = savedContact.get(i).id;
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int deleted_index = contactListUI.indexOfChild((View) v.getParent());
                    //RelativeLayout row_layout = (RelativeLayout) contactListUI.getChildAt(deleted_index);
                    //TextView tv_id = (TextView) row_layout.findViewById(R.id.tv_contactId);
                    //String del_id = tv_id.getText().toString();
                    db.deleteContact(contactID);
                    contactListUI.removeView(row);
                    contactSummarySetText(db.getAllContact().size());
                }
            });
            //add row to layout
            contactListUI.addView(row, i);
        }
    }


    private List<String> getGGaccounts(Context context) {
        //retrieve gg information
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(context).getAccounts();
        List<String> ret = new ArrayList<String>();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                if (account.type.equals("com.google")) {
                    ret.add(account.name);
                }
            }
        }
        return ret;
    }


    private void contactSummarySetText(int count) {
        if (count > 0)
            contactSummaryTextView.setText(count + " contacts added");
        else
            contactSummaryTextView.setText("No contact added");
    }


    // Projection array. Creating indices for this array instead of doing
// dynamic lookups improves performance.
    public static final String[] EVENT_PROJECTION = new String[]{
            CalendarContract.Calendars._ID,                           // 0
            CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,         // 1
            CalendarContract.Calendars.CALENDAR_COLOR                //2

    };

    // The indices for the projection array above.
    private static final int PROJECTION_ID_INDEX = 0;
    private static final int PROJECTION_DISPLAY_NAME_INDEX = 1;
    private static final int PROJECTION_CAL_COLOR = 2;


    public List<CalendarInfo> queryCalendar(Context context) {
        /*Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(context).getAccounts();
        String accName = "";
        for (Account account : accounts) {
            Log.wtf("Account", account.toString());

            if (emailPattern.matcher(account.name).matches()) {
                if (account.type.equals("com.google")) {
                    accName = account.name;
                }
            }
        }

        String selection = "((" + CalendarContract.Calendars.ACCOUNT_NAME + " = ?) AND ("
                + CalendarContract.Calendars.ACCOUNT_TYPE + " = ?))";

        String[] selectionArgs = new String[]{accName, "com.google"};*/

        // Submit the query and get a Cursor object back.

        List<CalendarInfo> ret = new ArrayList<CalendarInfo>();
        ContentResolver cr = mainView.getContext().getContentResolver();
        Cursor cur = null;
        Uri uri = CalendarContract.Calendars.CONTENT_URI;

        cur = cr.query(uri, EVENT_PROJECTION, null, null, null);
        // Use the cursor to step through the returned records
        while (cur.moveToNext()) {

            CalendarInfo temp = new CalendarInfo();

            long calID = 0;
            String displayName = null;


            // Get the field values
            calID = cur.getLong(PROJECTION_ID_INDEX);
            displayName = cur.getString(PROJECTION_DISPLAY_NAME_INDEX);
            String color = cur.getString(PROJECTION_CAL_COLOR);

            temp.color = color;
            temp.title = displayName;
            temp.id = calID;
            ret.add(temp);
        }
        return ret;
    }


}
