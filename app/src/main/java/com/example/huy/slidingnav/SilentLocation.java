package com.example.huy.slidingnav;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;


public class SilentLocation extends ActionBarActivity {

    private Toolbar toolbar;
    private ImageView imSetting, imCalendar, imClose;
    private GoogleMap currentLocationMap;
    private PagerAdapter pagerAdapter;
    private ViewPager viewPager;
    private SlidingTabLayout tabs;

    private final static int tabCount = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.silent_location);


        viewPager = (ViewPager) findViewById(R.id.pager_location);
        tabs = (SlidingTabLayout) findViewById(R.id.tabs_location);

        toolbar = (Toolbar) findViewById(R.id.toolbarLocation);
        toolbar.setTitleTextAppearance(this, R.style.MyTitleText);


        //toolbar button click
        imSetting = (ImageView) findViewById(R.id.imvSetting);
        imSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), Settings.class));
            }
        });

        imCalendar = (ImageView) findViewById(R.id.imvCalendar);
        imCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long startMillis = System.currentTimeMillis();
                Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
                builder.appendPath("time");
                ContentUris.appendId(builder, startMillis);
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(builder.build());
                startActivity(intent);

            }
        });

        //set toolbar bg color base on status
        if (MyPreference.preferenceReader(this, getResources().getString(R.string.location_status), "OFF").equals("ON")) {
            toolbar.setBackgroundColor(Color.parseColor("#5c20ff0b"));
            tabs.setBackgroundColor(Color.parseColor("#5c20ff0b"));

        } else {
            toolbar.setBackgroundColor(Color.parseColor("#4bff0a09"));
            tabs.setBackgroundColor(Color.parseColor("#4bff0a09"));

        }
        //set custom toolbar
        setSupportActionBar(toolbar);


        //populate nav drawer
        SlidingNav sliding_nav;
        sliding_nav = (SlidingNav) getSupportFragmentManager().findFragmentById(R.id.sliding_nav_frag);
        sliding_nav.display(R.id.sliding_nav_frag, (DrawerLayout) findViewById(R.id.drawer_layout_location), toolbar);


        //save pref
        SharedPreferences pref = getSharedPreferences("OPEN_MODE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("OPEN_MODE", "0");
        editor.commit();

        //set up pager;

        CharSequence Titles[] = {"Settings", "Pinned Location"};

        List<Fragment> tabFragment = new ArrayList<Fragment>();
        tabFragment.add(new SilentLocationTab1());
        tabFragment.add(new SilentLocationTab2());
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), Titles, tabFragment);
        viewPager.setAdapter(pagerAdapter);
        tabs.setDistributeEvenly(true);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                         @Override
                                         public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                         }

                                         @Override
                                         public void onPageSelected(int position) {
                                            /* if (position == 1)
                                                 if (getFragmentRefreshListener() != null) {
                                                     getFragmentRefreshListener().onRefresh();
                                                 }*/

                                         }

                                         @Override
                                         public void onPageScrollStateChanged(int state) {

                                         }
                                     }

        );


        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
                                       @Override
                                       public int getIndicatorColor(int position) {
                                           return getResources().getColor(R.color.tabsScrollColor);
                                       }
                                   }

        );
        tabs.setViewPager(viewPager);

        //get location service and display user loction on map
        /*LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .zoom(15).target(new LatLng(location.getLatitude(), location.getLongitude())).build();

                currentLocationMap.clear();
                currentLocationMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                currentLocationMap.addMarker(new MarkerOptions().
                        position(new LatLng(location.getLatitude(), location.getLongitude())).title("User's Location")).showInfoWindow();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0, locationListener);*/



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_silent_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
