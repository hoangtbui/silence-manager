package com.example.huy.slidingnav;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class SilentClassroomTab2 extends Fragment {


    private MyDB db;
    private ExpandableListView courseListView;

    private List<String> groups;
    private HashMap<String, List<CourseInfo>> courses_category;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.silent_classroom_tab2, container, false);
        db = new MyDB(mainView.getContext());

        courseListView = (ExpandableListView) mainView.findViewById(R.id.allCourseList);


        List<CourseInfo> allCourses = db.getAllCourse();

        groups = new ArrayList<String>();
        groups.add("Ended Courses");
        groups.add("In Progress Courses");
        groups.add("Future Courses");


        List<CourseInfo> inProgressCourses = new ArrayList<CourseInfo>();
        List<CourseInfo> endedCourses = new ArrayList<CourseInfo>();
        List<CourseInfo> futureCourses = new ArrayList<CourseInfo>();
        courses_category = new HashMap<String, List<CourseInfo>>();
        for (int i = 0; i < allCourses.size(); i++) {
            SimpleDateFormat format = new SimpleDateFormat("MM / dd / yyyy HH : mm");
            try {
                Date courseTimeStart = format.parse(allCourses.get(i).DtStart + " " + allCourses.get(i).TStart);
                Date courseTimeEnd = format.parse(allCourses.get(i).DtEnd + " " + allCourses.get(i).TEnd);
                Date currentTime = new Date();

                if (courseTimeStart.compareTo(currentTime) <= 0 && courseTimeEnd.compareTo(currentTime) >= 0) {
                    inProgressCourses.add(allCourses.get(i));
                    Log.wtf("inprogres", allCourses.get(i).title);
                } else if (courseTimeStart.compareTo(currentTime) > 0) {
                    futureCourses.add(allCourses.get(i));
                    Log.wtf("future", allCourses.get(i).title);
                } else if (courseTimeEnd.compareTo(currentTime) < 0) {
                    endedCourses.add(allCourses.get(i));
                    Log.wtf("end", allCourses.get(i).title);
                }

            } catch (Exception e) {

                Toast.makeText(mainView.getContext(), "Time parsing error", Toast.LENGTH_SHORT).show();
                e.printStackTrace();

            }
        }

        courses_category.put(groups.get(0), endedCourses);
        courses_category.put(groups.get(1) , inProgressCourses);
        courses_category.put(groups.get(2), futureCourses);

        ExpandableListAdapter  myAdapter = new ExpandableListAdapter(mainView, groups, courses_category);
        courseListView.setAdapter(myAdapter);

        return mainView;

    }
}