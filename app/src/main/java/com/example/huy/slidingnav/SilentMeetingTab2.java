package com.example.huy.slidingnav;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Pattern;
import java.util.zip.Inflater;

/**
 * Created by Huy on 7/24/2015.
 */
public class SilentMeetingTab2 extends Fragment {

    public View mainView;
    public MyDB db;
    public LinearLayout mainContainer;



    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {


        //refreshFragment(inflater,container);
        mainView = inflater.inflate(R.layout.silent_meeting_tab2, container, false);
        mainContainer = (LinearLayout) mainView.findViewById(R.id.tab2Container);
        db = new MyDB(mainView.getContext());

        ((SilentMeeting)getActivity()).setFragmentRefreshListener(new SilentMeeting.FragmentRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh Your Fragment
                Log.wtf("Refresh","after refreshing");
                mainContainer.removeAllViews();
                List<CalendarInfo> selectedCal = db.getAllCalendar();
                String[] idList = new String[selectedCal.size()];

                for (int i = 0; i < selectedCal.size(); i++) {
                    idList[i] = String.valueOf(selectedCal.get(i).id);
                    Log.wtf("Selected Cal", idList[i] + "");
                }
               /* Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                idList[selectedCal.size()] = String.valueOf(System.currentTimeMillis());*/


                List<EventInfo> events = queryEvent(mainView.getContext(), idList);

                for (int i = 0; i < events.size(); i++) {
                    View row = inflater.inflate(R.layout.event_row, null);
                    TextView name = (TextView) row.findViewById(R.id.tv_eventName);
                    name.setText(events.get(i).name);

                    View color = row.findViewById(R.id.eventColor);
                    color.setBackgroundColor(Integer.parseInt(events.get(i).color));

                    TextView start = (TextView) row.findViewById(R.id.tv_dateContainer);
                    String[] date = convertTime(events.get(i).dtstart, events.get(i).timezone, "dd/MMM/yyyy").split("/");
                    start.setText(date[0] + " " + date[1] + "\n" + date[2]);

                    TextView time = (TextView) row.findViewById(R.id.tv_eventTime);

                    if (events.get(i).allday.equals("1"))
                    {
                        time.setText("All day");
                    }
                    else
                    {
                        String dateEnd = convertTime(events.get(i).dtend, events.get(i).timezone, "dd MMM yy");
                        time.setText(convertTime(events.get(i).dtstart, events.get(i).timezone, "HH:mm") + " - "
                                + convertTime(events.get(i).dtend, events.get(i).timezone, "HH:mm") + " (" + dateEnd + ")");
                    }

                    mainContainer.addView(row);
                }
            }
        });


        return mainView;

    }





    public String convertTime(String epoch, String timezone, String style) {
        if (epoch != null && timezone != null && style != null) {
            try {
                Date date = new Date(Long.parseLong(epoch));
                SimpleDateFormat format = new SimpleDateFormat(style);
                format.setTimeZone(TimeZone.getTimeZone(timezone));
                String formatted = format.format(date);
                return formatted;
            } catch (Exception e) {
                Toast.makeText(getActivity(), "Error converting date/time", Toast.LENGTH_SHORT).show();
            }

        }
        return "-1";
    }


    // Projection array. Creating indices for this array instead of doing
// dynamic lookups improves performance.
    public static final String[] EVENT_PROJECTION = new String[]{
            CalendarContract.Events.CALENDAR_ID,
            CalendarContract.Events.TITLE,
            CalendarContract.Events.DTSTART,
            CalendarContract.Events.DTEND,
            CalendarContract.Events.EVENT_TIMEZONE,
            CalendarContract.Calendars.CALENDAR_COLOR,
            CalendarContract.Events.ALL_DAY
    };

    // The indices for the projection array above.
    private static final int PROJECTION_CALENDAR_ID = 0;
    private static final int PROJECTION_EVENT_NAME = 1;
    private static final int PROJECTION_DTSTART = 2;
    private static final int PROJECTION_DTEND = 3;
    private static final int PROJECTION_TIMEZONE = 4;
    private static final int PROJECTION_EVENT_COLOR = 5;
    private static final int PROJECTION_EVENT_ALLDAY = 6;


    public List<EventInfo> queryEvent(Context context, String[] id) {
       /* Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(context).getAccounts();
        String accName = "";
        for (Account account : accounts) {
            Log.wtf("Account", account.toString());

            if (emailPattern.matcher(account.name).matches()) {
                if (account.type.equals("com.google")) {
                    accName = account.name;
                }
            }
        }*/
        List<EventInfo> ret = new ArrayList<EventInfo>();

        Cursor cur = null;
        ContentResolver cr = mainView.getContext().getContentResolver();
        Uri uri = CalendarContract.Events.CONTENT_URI;

       /* String selection = "((" + CalendarContract.Calendars.ACCOUNT_NAME + " = ?) AND ("
                + CalendarContract.Calendars.ACCOUNT_TYPE + " = ?) AND (" + CalendarContract.Events.CALENDAR_ID + " = ?))";
        String[] selectionArgs = new String[]{accName, "com.google", id};*/

        //get current time
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        //create selection string
        String selection =  CalendarContract.Events.CALENDAR_ID + " IN (?";

        int count = 1;
        while (count < id.length) {
            selection += ",?";
            count++;
        }
        selection += ")";
       // selection += ") AND "+ CalendarContract.Events.DTSTART + " <= ?";
        Log.wtf("selection", selection);

        //String[] selectionArgs = new String[]{id};
        // Submit the query and get a Cursor object back.

        cur = cr.query(uri, EVENT_PROJECTION, selection, id, "dtstart ASC");
        // Use the cursor to step through the returned records
        while (cur.moveToNext()) {

            if (Long.parseLong(cur.getString(PROJECTION_DTSTART)) <= System.currentTimeMillis())
                continue;
            EventInfo temp = new EventInfo();
            temp.id = cur.getString(PROJECTION_CALENDAR_ID);
            temp.name = cur.getString(PROJECTION_EVENT_NAME);
            temp.color = cur.getString(PROJECTION_EVENT_COLOR);
            temp.dtstart = cur.getString(PROJECTION_DTSTART);
            temp.dtend = cur.getString(PROJECTION_DTEND);
            temp.timezone = cur.getString(PROJECTION_TIMEZONE);
            temp.allday = cur.getString(PROJECTION_EVENT_ALLDAY);
            ret.add(temp);

        }

        return ret;
    }


}
