package com.example.huy.slidingnav;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Huy on 7/17/2015.
 */
public class MyPreference {


    public static String preferenceReader(Context context, String prefName, String defValue) {
        SharedPreferences pref = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        return pref.getString(prefName, defValue);
    }


    public static void preferenceWriter(Context context, String prefName, String value) {
        SharedPreferences pref = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(prefName, value);
        editor.commit();
    }
}
