package com.example.huy.slidingnav;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Huy on 7/20/2015.
 */
public class MyDB extends SQLiteOpenHelper {

    public static final String DB_NAME = "mydb";
    public static int DB_VERSION = 1;
    public static String SELECTED_CONTACT_TABLE = "SelectedContacts";
    public static String SELECTED_CONTACT_ID = "contact_id";
    public static String SELECTED_CONTACT_NAME = "contact_name";
    public static String SELECTED_CONTACT_NUMBER = "contact_number";

    public static String SELECTED_CALENDAR_TABLE = "SelectedCalendars";
    public static String SELECTED_CALENDAR_ID = "calendar_id";
    public static String SELECTED_CALENDAR_NAME = "calendar_name";

    public static String COURSE_TABLE = "course";

    public static String CREATE_CONTACT_TABLE =
            "create table if not exists SelectedContacts (_id integer primary key, " +
                    "contact_id integer, contact_name text, contact_number text)";
    public static String DROP_CONTACT_TABLE =
            "drop table if exists SelectedContacts";

    public static String CREATE_SELECTED_CALENDAR_TABLE =
            "create table if not exists SelectedCalendars (_id integer primary key, calendar_id integer , calendar_name text)";

    public static String DROP_SELECTED_CALENDAR_TABLE = "drop table if exists SelectedCalendars";

    public  static String CREATE_COURSE_TABLE = "create table if not exists course (_id integer primary key, title text, dstart text, dend text ,tstart text, tend text, dow text)";

    public static String DROP_COURSE_TABLE = "drop table if exists course";

    public MyDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropContactTable();
        dropCalendarTable();
        dropCourseTable();
        onCreate(db);
    }

    public void dropCalendarTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(DROP_SELECTED_CALENDAR_TABLE);
    }

    public void createCalendarTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(CREATE_SELECTED_CALENDAR_TABLE);
    }


    public void dropContactTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(DROP_CONTACT_TABLE);
    }

    public void createContactTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(CREATE_CONTACT_TABLE);
    }

    public void insertContact(String id, String name, String number) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put(SELECTED_CONTACT_ID, id);
        content.put(SELECTED_CONTACT_NAME, name);
        content.put(SELECTED_CONTACT_NUMBER, number);
        db.insert(SELECTED_CONTACT_TABLE, null, content);
    }

    public Integer deleteContact(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(SELECTED_CONTACT_TABLE, SELECTED_CONTACT_ID + " = ? ", new String[]{id});
    }


    public void insertCalendar(String id, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        /*ContentValues content = new ContentValues();
        content.put(SELECTED_CALENDAR_ID, id);
        content.put(SELECTED_CALENDAR_NAME, name);

        db.insert(SELECTED_CALENDAR_TABLE, null, content);*/
        db.execSQL("insert into " + SELECTED_CALENDAR_TABLE + " (" + SELECTED_CALENDAR_ID + "," + SELECTED_CALENDAR_NAME + ") " +
                "select " + id + ",'" + name + "' where not exists (select 1 from "+ SELECTED_CALENDAR_TABLE + " WHERE " + SELECTED_CALENDAR_ID + " = '" + id +"')");
    }

    public Integer deleteCalendar(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(SELECTED_CALENDAR_TABLE, SELECTED_CALENDAR_ID + " = ? ", new String[]{id});
    }


    public void insertCourse (String title, String dstart, String dend, String tstart, String tend, String [] dow)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("title", title);
        content.put("dstart", dstart);
        content.put("dend", dend);
        content.put("tstart", tstart);
        content.put("tend", tend);
        String temp="";

        for (int i = 0; i < 7;i++)
        {
            if (dow[i].equals("1"))
                temp += "1";
            else
                temp += "0";
            if (i+1 < dow.length)
                temp += ",";

        }
        content.put("dow", temp);

        db.insert(COURSE_TABLE, null, content);

    }

    public Integer deleteCourse (String id){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("course",  "_id = ? ", new String[]{id});
    }


    public void createCourseTable (){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(CREATE_COURSE_TABLE);
    }

    public void dropCourseTable()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(DROP_COURSE_TABLE);
    }

    public List<CourseInfo> getAllCourse() {
        List<CourseInfo> ret = new ArrayList<CourseInfo>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("select * from course", null);
        cur.moveToFirst();

        while (cur.isAfterLast() == false) {
            CourseInfo temp = new CourseInfo();
            temp.id = cur.getString(cur.getColumnIndex("_id"));
            temp.title = cur.getString(cur.getColumnIndex("title"));
            temp.DtStart = cur.getString(cur.getColumnIndex("dstart"));
            temp.DtEnd = cur.getString(cur.getColumnIndex("dend"));
            temp.TStart = cur.getString(cur.getColumnIndex("tstart"));
            temp.TEnd = cur.getString(cur.getColumnIndex("tend"));

            String [] dow = cur.getString(cur.getColumnIndex("dow")).split(",");

            for (int i = 0; i < 7;i++){
                if (dow[i].equals("1"))
                {
                    temp.dow[i]="1";
                }

            }


            ret.add(temp);
            cur.moveToNext();
        }
        return ret;
    }


    public List<ContactInfo> getAllContact() {
        List<ContactInfo> ret = new ArrayList<ContactInfo>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("select * from SelectedContacts", null);
        cur.moveToFirst();

        while (cur.isAfterLast() == false) {
            ContactInfo temp = new ContactInfo();
            temp.number = cur.getString(cur.getColumnIndex(SELECTED_CONTACT_NUMBER));
            temp.name = cur.getString(cur.getColumnIndex(SELECTED_CONTACT_NAME));
            temp.id = cur.getString(cur.getColumnIndex(SELECTED_CONTACT_ID));

            ret.add(temp);
            cur.moveToNext();
        }
        return ret;
    }


    public List<CalendarInfo> getAllCalendar (){
        List<CalendarInfo> ret = new ArrayList<CalendarInfo>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("select * from SelectedCalendars", null);
        cur.moveToFirst();
        while (cur.isAfterLast() == false) {
            CalendarInfo temp = new CalendarInfo();
            temp.id = Long.parseLong(cur.getString(cur.getColumnIndex(SELECTED_CALENDAR_ID)));
            temp.title = cur.getString(cur.getColumnIndex(SELECTED_CALENDAR_NAME));

            ret.add(temp);
            cur.moveToNext();
        }
        return ret;
    }

}
