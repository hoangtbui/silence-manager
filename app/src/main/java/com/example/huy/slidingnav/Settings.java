package com.example.huy.slidingnav;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;


public class Settings extends ActionBarActivity {
    private Toolbar toolbar;
    private Switch smSwitch, slSwitch, scSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        toolbar = (Toolbar) findViewById(R.id.toolbar_setting);
        toolbar.setTitleTextAppearance(this, R.style.MyTitleText);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(Color.parseColor("#5c20ff0b"));


        //set back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        smSwitch = (Switch) findViewById(R.id.silentMeetingSwitch);
        slSwitch = (Switch) findViewById(R.id.silentLocationSwitch);
        scSwitch = (Switch) findViewById(R.id.silentClassroomSwitch);

        if (MyPreference.preferenceReader(this, getResources().getString(R.string.meeting_status), "OFF").equals("ON")) {
            smSwitch.setChecked(true);
        } else smSwitch.setChecked(false);

        if (MyPreference.preferenceReader(this, getResources().getString(R.string.classroom_status), "OFF").equals("ON")) {
            scSwitch.setChecked(true);
        } else scSwitch.setChecked(false);

        smSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    MyPreference.preferenceWriter(buttonView.getContext(), getResources().getString(R.string.meeting_status), "ON");
                } else {
                    MyPreference.preferenceWriter(buttonView.getContext(), getResources().getString(R.string.meeting_status), "OFF");


                }
            }
        });

        scSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    MyPreference.preferenceWriter(buttonView.getContext(), getResources().getString(R.string.classroom_status), "ON");
                } else {
                    MyPreference.preferenceWriter(buttonView.getContext(), getResources().getString(R.string.classroom_status), "OFF");


                }
            }
        });

        slSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true) {
                    MyPreference.preferenceWriter(buttonView.getContext(), getResources().getString(R.string.location_status), "ON");
                } else {
                    MyPreference.preferenceWriter(buttonView.getContext(), getResources().getString(R.string.location_status), "OFF");


                }
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long-+
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
