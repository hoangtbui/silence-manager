package com.example.huy.slidingnav;



import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;


public class SilentMeeting extends ActionBarActivity {

    private Toolbar toolbar;
    private ImageView imSetting, imCalendar, imClose;


    private PagerAdapter pagerAdapter;
    private ViewPager viewPager;
    private SlidingTabLayout tabs;

    private final static int tabCount = 2;

    public interface FragmentRefreshListener {
        void onRefresh();
    }

    private FragmentRefreshListener fragmentRefreshListener;

    public FragmentRefreshListener getFragmentRefreshListener() {
        return fragmentRefreshListener;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener fragmentRefreshListener) {
        this.fragmentRefreshListener = fragmentRefreshListener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CharSequence Titles[] = {"Settings", "Upcoming Events"};

        setContentView(R.layout.silent_meeting);
        toolbar = (Toolbar) findViewById(R.id.toolbarMeeting);
        toolbar.setTitleTextAppearance(this, R.style.MyTitleText);

        viewPager = (ViewPager) findViewById(R.id.pager_meeting);
        tabs = (SlidingTabLayout) findViewById(R.id.tabs_meeting);


        //toolbar button click
        imSetting = (ImageView) findViewById(R.id.imvSetting);
        imSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), Settings.class));
            }
        });

        imCalendar = (ImageView) findViewById(R.id.imvCalendar);
        imCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long startMillis = System.currentTimeMillis();
                Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
                builder.appendPath("time");
                ContentUris.appendId(builder, startMillis);
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(builder.build());
                startActivity(intent);

            }
        });

        //set toolbar bg color base on status
        if (MyPreference.preferenceReader(this, getResources().getString(R.string.meeting_status), "OFF").equals("ON")) {
            toolbar.setBackgroundColor(Color.parseColor("#5c20ff0b"));
            tabs.setBackgroundColor(Color.parseColor("#5c20ff0b"));
        } else {
            toolbar.setBackgroundColor(Color.parseColor("#60ff0a09"));
            tabs.setBackgroundColor(Color.parseColor("#60ff0a09"));
        }
        //set custom toolbar
        setSupportActionBar(toolbar);


        //populate nav drawer
        SlidingNav sliding_nav;
        sliding_nav = (SlidingNav) getSupportFragmentManager().findFragmentById(R.id.sliding_nav_frag);
        sliding_nav.display(R.id.sliding_nav_frag, (DrawerLayout) findViewById(R.id.drawer_layout_meeting), toolbar);


        //save pref
        SharedPreferences pref = getSharedPreferences("OPEN_MODE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("OPEN_MODE", "0");
        editor.commit();


        //set up pager;
        List<Fragment> tabFragment = new ArrayList<Fragment>();
        tabFragment.add(new SilentMeetingTab1());
        tabFragment.add(new SilentMeetingTab2());
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), Titles, tabFragment);
        viewPager.setAdapter(pagerAdapter);
        tabs.setDistributeEvenly(true);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                         @Override
                                         public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                         }

                                         @Override
                                         public void onPageSelected(int position) {
                                             if (position == 1)
                                                 if (getFragmentRefreshListener() != null) {
                                                     getFragmentRefreshListener().onRefresh();
                                                 }

                                         }

                                         @Override
                                         public void onPageScrollStateChanged(int state) {

                                         }
                                     }

        );


        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer()

                                   {
                                       @Override
                                       public int getIndicatorColor(int position) {
                                           return getResources().getColor(R.color.tabsScrollColor);
                                       }
                                   }

        );
        tabs.setViewPager(viewPager);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_silentmeeting, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

}
