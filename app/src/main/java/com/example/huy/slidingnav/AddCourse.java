package com.example.huy.slidingnav;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class AddCourse extends ActionBarActivity {


    private TextView startDate, endDate, startTime, endTime;
    private Toolbar toolbar;
    private MyDB db;
    private Button submitCourseButton;
    private EditText courseTitleEditText;
    private String coursename, sDstart, sDend, sTstart, sTend;
    private final String timePattern = "HH : mm";
    private final String datePattern = "MM / dd / yyyy";
    private String[] dow = new String[]{"0", "0", "0", "0", "0", "0", "0"};
    private CheckBox monCb, tueCb, wedCb, thurCb, friCb, satCb, sunCb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_course);

        submitCourseButton = (Button) findViewById(R.id.submitCourse);
        db = new MyDB(this);


        toolbar = (Toolbar) findViewById(R.id.toolbarAddCourse);
        toolbar.setTitleTextAppearance(this, R.style.MyTitleText);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        startDate = (TextView) findViewById(R.id.startDate);
        startTime = (TextView) findViewById(R.id.startTime);
        endDate = (TextView) findViewById(R.id.endDate);
        endTime = (TextView) findViewById(R.id.endTime);
        courseTitleEditText = (EditText) findViewById(R.id.courseName);
        monCb = (CheckBox) findViewById(R.id.monCb);
        tueCb = (CheckBox) findViewById(R.id.tueCb);
        wedCb = (CheckBox) findViewById(R.id.wedCb);
        thurCb = (CheckBox) findViewById(R.id.thurCb);
        friCb = (CheckBox) findViewById(R.id.friCb);
        satCb = (CheckBox) findViewById(R.id.satCb);
        sunCb = (CheckBox) findViewById(R.id.sunCb);

        final Calendar c = Calendar.getInstance();
        int h = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);
        int y = c.get(Calendar.YEAR);
        int m = c.get(Calendar.MONTH);
        int d = c.get(Calendar.DAY_OF_MONTH);


        m++;
        if (m < 10) {
            sDstart = "0" + m;
            sDend = "0" + m;
        } else {
            sDstart = sDstart + m;
            sDend = sDend + m;
        }
        sDstart += " / ";
        sDend += " / ";


        if (d < 10) {
            sDstart = sDstart + "0" + d;
            sDend = sDend + "0" + d;
        } else {
            sDstart = sDstart + d + "";
            sDend = sDend + d + "";
        }

        sDstart += " / " + y;
        sDend += " / " + y;

        if (h < 10) {
            sTstart = "0" + h;
            sTend = "0" + h;
        } else {
            sTstart = "" + h;
            sTend = "" + h;
        }

        sTstart += " : ";
        sTend += " : ";
        if (min < 10) {
            sTstart += ("0" + min);
            sTend += ("0" + min);
        } else {
            sTstart += ("" + min);
            sTend += ("" + min);
        }


        startDate.setText(sDstart);
        endDate.setText(sDend);
        startTime.setText(sTstart);
        endTime.setText(sTend);


        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment startDatePicker = new DatePickerFragment() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        //super.onDateSet(view, year, month, day);
                        String m, d;
                        month++;
                        if (month < 10)
                            m = "0" + month;
                        else
                            m = month + "";
                        if (day < 10)
                            d = "0" + day;
                        else
                            d = day + "";
                        sDstart = m + " / " + d + " / " + year;
                        startDate.setText(sDstart);
                    }
                };
                startDatePicker.show(getSupportFragmentManager(), "datePicker");
            }
        });

        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment startDatePicker = new DatePickerFragment() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        //super.onDateSet(view, year, month, day);
                        String m, d;
                        month++;
                        if (month < 10)
                            m = "0" + month;
                        else
                            m = month + "";
                        if (day < 10)
                            d = "0" + day;
                        else
                            d = day + "";
                        sDend = m + " / " + d + " / " + year;
                        endDate.setText(sDend);
                    }
                };
                startDatePicker.show(getSupportFragmentManager(), "datePicker");
            }
        });

        startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment startDatePicker = new TimePickerFragment() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String h, m;

                        if (hourOfDay < 10)
                            h = "0" + hourOfDay;
                        else
                            h = hourOfDay + "";
                        if (minute < 10)
                            m = "0" + minute;
                        else
                            m = minute + "";
                        sTstart = h + " : " + m;
                        startTime.setText(sTstart);
                    }
                };
                startDatePicker.show(getSupportFragmentManager(), "datePicker");
            }
        });

        endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment startDatePicker = new TimePickerFragment() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String h, m;

                        if (hourOfDay < 10)
                            h = "0" + hourOfDay;
                        else
                            h = hourOfDay + "";
                        if (minute < 10)
                            m = "0" + minute;
                        else
                            m = minute + "";
                        sTend = h + " : " + m;
                        endTime.setText(sTend);
                    }
                };
                startDatePicker.show(getSupportFragmentManager(), "datePicker");
            }
        });


        //get dow from checkbox
        for (int a = 0; a < dow.length; a++) {
            dow[a] = "0";
        }
        CheckBox.OnClickListener cbListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = v.getId();
                switch (id) {
                    case R.id.monCb:
                        if (((CheckBox) v).isChecked())
                            dow[1] = "1";
                        break;
                    case R.id.tueCb:
                        if (((CheckBox) v).isChecked())
                            dow[2] = "1";
                        break;
                    case R.id.wedCb:
                        if (((CheckBox) v).isChecked())
                            dow[3] = "1";
                        break;
                    case R.id.thurCb:
                        if (((CheckBox) v).isChecked())
                            dow[4] = "1";
                        break;
                    case R.id.friCb:
                        if (((CheckBox) v).isChecked())
                            dow[5] = "1";
                        break;
                    case R.id.satCb:
                        if (((CheckBox) v).isChecked())
                            dow[6] = "1";
                        break;
                    case R.id.sunCb:
                        if (((CheckBox) v).isChecked())
                            dow[0] = "1";
                        break;

                }
            }
        };
        monCb.setOnClickListener(cbListener);
        tueCb.setOnClickListener(cbListener);
        wedCb.setOnClickListener(cbListener);
        thurCb.setOnClickListener(cbListener);
        friCb.setOnClickListener(cbListener);
        satCb.setOnClickListener(cbListener);
        sunCb.setOnClickListener(cbListener);


        //submit new course
        submitCourseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat timeFormat = new SimpleDateFormat(timePattern);
                SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
                coursename = courseTitleEditText.getText().toString();

                int checkBoxCount = 0;
                for (int c = 0; c < dow.length; c++) {
                    if (dow[c].equals("1")) {
                        checkBoxCount++;
                        break;

                    }
                }

                try {
                    Date timestart = timeFormat.parse(sTstart);
                    Date timeend = timeFormat.parse(sTend);
                    Date datestart = dateFormat.parse(sDstart);
                    Date dateend = dateFormat.parse(sDend);
                    //compare time and date

                    if (coursename.equals("")) {
                        showAlertDialog("Please provide your course name", v);
                    } else if (datestart.compareTo(dateend) >= 0) {
                        showAlertDialog("Your date range is inappropriate", v);
                    } else if (timestart.compareTo(timeend) >= 0) {
                        showAlertDialog("Your time range is inappropriate", v);
                    } else if (checkBoxCount == 0) {
                        showAlertDialog("Please selects days of week for your class", v);
                    } else {
                        db.insertCourse(coursename, sDstart, sDend, sTstart, sTend, dow);
                        List<CourseInfo> course = db.getAllCourse();
                        for (int p = 0; p < course.size(); p++) {
                            Log.wtf("course", course.get(p).title + " " + course.get(p).id + " " + course.get(p).DtStart + " " + course.get(p).DtEnd + " " + course.get(p).TStart + " " + course.get(p).TEnd);
                        }

                        startActivity(new Intent(v.getContext(), SilentClassroom.class));
                    }
                } catch (Exception e) {
                    Toast.makeText(v.getContext(), "Error parsing time", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        });


    }


    void showAlertDialog(String message, View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setTitle("Warning");
        builder.setMessage(message);
        builder.setPositiveButton("Accept", null);
        AlertDialog warn = builder.create();
        warn.show();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_course, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
