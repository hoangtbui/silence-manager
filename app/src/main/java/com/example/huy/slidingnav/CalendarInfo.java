package com.example.huy.slidingnav;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created by Huy on 7/11/2015.
 */
public class CalendarInfo {

    public String title = "";
    public String color = "";
    public boolean selected = false;
    public long id;

}

