package com.example.huy.slidingnav;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CalendarListviewAdapter extends ArrayAdapter<CalendarInfo> {

    private List<CalendarInfo> calendarList;
    private Context context;


    public CalendarListviewAdapter(Context context, List<CalendarInfo> resource) {
        super(context, R.layout.calendar_list_row, resource);
        this.context=context;
        calendarList=resource;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null)
        {
            LayoutInflater inflater  = LayoutInflater.from(this.getContext());
            convertView = inflater.inflate(R.layout.calendar_list_row,parent, false);

        }

        CalendarInfo calInfo = calendarList.get(position);

        View color = convertView.findViewById(R.id.calendar_color);
        color.setBackgroundColor(Integer.parseInt(calInfo.color));

        TextView title = (TextView) convertView.findViewById(R.id.calendar_title);
        title.setText(calInfo.title);
        return convertView;
    }
}
