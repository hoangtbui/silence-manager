package com.example.huy.slidingnav;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class SilentClassroomTab1 extends Fragment {

    private Spinner minSpinner, hourSpinner;
    private ImageView addCourseButton;
    private LinearLayout inProgressCourseUI;
    private MyDB db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.silent_classroom_tab1, container, false);
        db = new MyDB(mainView.getContext());
        db.createCourseTable();

        inProgressCourseUI = (LinearLayout) mainView.findViewById(R.id.inProgressCourseContainer);

        minSpinner = (Spinner) mainView.findViewById(R.id.minSpinnerClassroom);
        List<String> minute = new ArrayList<String>();
        for (int i = 0; i <= 59; i++) {
            minute.add(i + "");
        }
        ArrayAdapter<String> spinnerAdapater = new ArrayAdapter<String>(mainView.getContext(), R.layout.simple_row, R.id.rowTV, minute);
        minSpinner.setAdapter(spinnerAdapater);

        addCourseButton = (ImageView) mainView.findViewById(R.id.addCourseButton);
        addCourseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), AddCourse.class));
            }
        });


        // display list of active courses
        List<CourseInfo> courses = db.getAllCourse();
        for (int i = 0; i < courses.size(); i++) {
            SimpleDateFormat format = new SimpleDateFormat("MM / dd / yyyy HH : mm");
            try {
                Date courseTimeStart = format.parse(courses.get(i).DtStart + " " + courses.get(i).TStart);
                Date courseTimeEnd = format.parse(courses.get(i).DtEnd + " " + courses.get(i).TEnd);
                Date currentTime = new Date();

                if (courseTimeStart.compareTo(currentTime) <= 0 && courseTimeEnd.compareTo(currentTime) >= 0) {
                    View row = LayoutInflater.from(mainView.getContext()).inflate(R.layout.course_mini_row, null);
                    TextView name, timestart, timeend;
                    //inflate row layout

                    //show title
                    name = (TextView) row.findViewById(R.id.miniRowCourseName);
                    name.setText(courses.get(i).title.toUpperCase());

                    timestart = (TextView) row.findViewById(R.id.miniRowCourseTime);

                    //show  time
                    String hour[] = courses.get(i).TStart.split(" : ");
                    String ap = (Integer.parseInt(hour[0]) / 12 == 0 ? "AM" : "PM");
                    String formatedTime = "";
                    if (Integer.parseInt(hour[0]) % 12 != 0)
                        formatedTime = Integer.parseInt(hour[0]) % 12 + " : " + hour[1] + " " + ap;
                    else
                        formatedTime = "12 : " + hour[1] + " " + ap;

                    formatedTime += " - ";

                    hour = courses.get(i).TEnd.split(" : ");
                    ap = (Integer.parseInt(hour[0]) / 12 == 0 ? "AM" : "PM");
                    if (Integer.parseInt(hour[0]) % 12 != 0)
                        formatedTime += Integer.parseInt(hour[0]) % 12 + " : " + hour[1] + " " + ap;
                    else
                        formatedTime += "12 : " + hour[1] + " " + ap;
                    timestart.setText(formatedTime);


                    TextView dowTv = (TextView) row.findViewById(R.id.dowTV);


                    //check for next day of class
                    String nextClassTime = "";
                    Calendar currentCal = Calendar.getInstance();
                    currentCal.setTime(new Date());
                    Calendar courseCal = Calendar.getInstance();
                    courseCal.set(Calendar.MONTH, currentCal.get(Calendar.MONTH));
                    courseCal.set(Calendar.YEAR, currentCal.get(Calendar.YEAR));
                    courseCal.set(Calendar.WEEK_OF_MONTH, currentCal.get(Calendar.WEEK_OF_MONTH));

                    Date predictedNextDate = new Date();
                    boolean found = false;
                    int weekIncrement = 0;
                    while (!found) {
                        courseCal.set(Calendar.WEEK_OF_MONTH, currentCal.get(Calendar.WEEK_OF_MONTH) + weekIncrement);
                        for (int x = 0; x < courses.get(i).dow.length; x++) {
                            if (courses.get(i).dow[x].equals("1")) {

                                    courseCal.set(Calendar.DAY_OF_WEEK, x+1);
                                Log.wtf(courses.get(i).title, courseCal.getTime().toString());

                                String timeString = "";
                                int temp_date = courseCal.get(Calendar.DAY_OF_MONTH);
                                int temp_month = courseCal.get(Calendar.MONTH) + 1;
                                timeString += temp_month + " / " + temp_date + " / " + courseCal.get(Calendar.YEAR) + " " + courses.get(i).TStart;

                                SimpleDateFormat format_1 = new SimpleDateFormat("MM / dd / yyyy HH : mm");
                                Date currentCourseDateFound = format_1.parse(timeString);
                                if (currentCourseDateFound.compareTo(currentTime) < 0) {
                                    //increment 1 week
                                /*courseCal.set(Calendar.WEEK_OF_MONTH, currentCal.get(Calendar.WEEK_OF_MONTH) + weekIncrement);
                                Log.wtf(courses.get(i).title, courseCal.getTime().toString());

                                 temp_date = courseCal.get(Calendar.DAY_OF_MONTH) ;
                                 temp_month = courseCal.get(Calendar.MONTH) + 1;
                                timeString += temp_month + " / " + temp_date + " / " + courseCal.get(Calendar.YEAR) + " " + courses.get(i).TStart;
                                SimpleDateFormat format_2 = new SimpleDateFormat("dd MMM");
                                dowTv.setText(getDow(courseCal.get(Calendar.DAY_OF_WEEK)) + "\n" + format_2.format(format_1
                                        .parse(timeString)));*/
                                    continue;

                                } else if (currentCourseDateFound.compareTo(currentTime) >= 0) {
                                    predictedNextDate = (currentCourseDateFound);
                                    found = true;

                                    temp_date = courseCal.get(Calendar.DAY_OF_MONTH);
                                    temp_month = courseCal.get(Calendar.MONTH) + 1;
                                    timeString += temp_month + " / " + temp_date + " / " + courseCal.get(Calendar.YEAR) + " " + courses.get(i).TStart;
                                    SimpleDateFormat format_2 = new SimpleDateFormat("dd MMM");
                                    dowTv.setText(getDow(courseCal.get(Calendar.DAY_OF_WEEK)) + "\n" + format_2.format(format_1
                                            .parse(timeString)));
                                    break;
                                /*SimpleDateFormat format_2 = new SimpleDateFormat("dd MMM");
                                dowTv.setText(getDow(courseCal.get(Calendar.DAY_OF_WEEK)) + "\n" + format_2.format(currentCourseDateFound));*/
                                }
                            }
                        }
                        weekIncrement++;
                    }


                    //end check next day of class

                    View div = row.findViewById(R.id.miniRowDiv);
                    if (i + 1 >= courses.size())
                        div.setVisibility(View.INVISIBLE);
                    //add row to layout
                    inProgressCourseUI.addView(row);


                } else //if class passed end date, continue loop
                    continue;

            } catch (ParseException e) {
                Toast.makeText(mainView.getContext(), "Time parsing error", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
        return mainView;
    }


    public String getDow(int i) {
        String ret = "";
        if (i == 2)
            ret = "Mon";
        else if (i == 3)
            ret = "Tue";
        else if (i == 4)
            ret = "Wed";
        else if (i == 5)
            ret = "Thu";
        else if (i == 6)
            ret = "Fri";
        else if (i == 7)
            ret = "Sat";
        else if (i == 1)
            ret = "Sun";
        return ret;
    }


}//end class
