package com.example.huy.slidingnav;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Huy on 8/12/2015.
 */
public class SilentLocationTab1 extends Fragment {

    private Spinner minSpinner;
    private GoogleMap map;
    private ImageView addBt;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View mainView = inflater.inflate(R.layout.silent_location_tab1, container, false);


        minSpinner = (Spinner) mainView.findViewById(R.id.minSpinnerLocation);
        List<String> minute = new ArrayList<String>();
        for (int i = 0; i <= 59; i++) {
            minute.add(i + "");
        }
        ArrayAdapter<String> spinnerAdapater = new ArrayAdapter<String>(mainView.getContext(), R.layout.simple_row, R.id.rowTV, minute);
        minSpinner.setAdapter(spinnerAdapater);

        addBt = (ImageView) mainView.findViewById(R.id.addLocationBt);
        addBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mainView.getContext(), SelectLocation.class));
            }


        });
        return mainView;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
}
