package com.example.huy.slidingnav;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;


public class SelectLocation extends ActionBarActivity {


    private CheckBox sattleLiteCb;
    private GoogleMap map;
    private ImageView addBt;
    private List<Marker> locationList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_location);

        addBt = (ImageView) findViewById(R.id.addLocationBt);
        locationList = new ArrayList<Marker>();

        if (map == null)
            map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

        //sattlelite mode
        sattleLiteCb = (CheckBox) findViewById(R.id.cb_sattlelite);
        sattleLiteCb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                } else map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            }
        });


        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        String locationProvider = LocationManager.NETWORK_PROVIDER;
        // Or use LocationManager.GPS_PROVIDER
        final Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .zoom(15).target(new LatLng(lastKnownLocation.getLatitude(),lastKnownLocation.getLongitude())).build();
        //zoom to last known location w a marker
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        map.addMarker(new MarkerOptions().
                position(new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude())).title("Your Current/Last Known Location")).showInfoWindow();



        addBt = (ImageView) findViewById(R.id.bt_pinLocation);
        final Geocoder geocoder = new Geocoder(this , Locale.getDefault());

        //view info window when click on marker
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                LatLng pos = marker.getPosition();

                List<Address> addresses;


                try {
                    addresses = geocoder.getFromLocation(pos.latitude, pos.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                    marker.setTitle(address + ", "+ city + ", " + state + ", " + country );
                    marker.showInfoWindow();


                } catch (IOException e) {
                    e.printStackTrace();
                }


                return false;
            }
        });

        //this will make marker update position
        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {

            }
        });


        //add new pinpoint
        addBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random = new Random();
                Marker marker = map.addMarker(new MarkerOptions().
                        position(new LatLng(lastKnownLocation.
                                getLatitude() + (random.nextFloat() / 200), lastKnownLocation.getLongitude() + (random.nextFloat() / 200))).draggable(true)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pinpoint)));


                locationList.add(marker);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_select_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
