package com.example.huy.slidingnav;

/**
 * Created by Huy on 7/25/2015.
 */
public class EventInfo {
    public String name;
    public String id;
    public String dtstart;
    public String dtend;
    public String color;
    public String timezone;
    public String allday;

}
