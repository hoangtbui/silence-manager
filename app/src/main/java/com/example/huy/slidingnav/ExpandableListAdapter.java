package com.example.huy.slidingnav;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Huy on 8/3/2015.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private View mainView;

    private HashMap<String, List<CourseInfo>> groupItems;
    private List<String> groupname;
    private LayoutInflater inflater;

    public ExpandableListAdapter(View mainView, List<String> groupname, HashMap<String, List<CourseInfo>> groupItems) {

        this.mainView = mainView;
        this.groupItems = groupItems;
        this.groupname = groupname;
        inflater = (LayoutInflater) mainView.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getGroupCount() {
        return groupItems.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {


        return groupItems.get(groupname.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupname.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groupItems.get(groupname.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.group_exlv, parent, false);
        }
        TextView name = (TextView) convertView.findViewById(R.id.elv_groupName);
        name.setText(((String) getGroup(groupPosition)).toUpperCase());
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, final ViewGroup parent) {

        final CourseInfo course = (CourseInfo) getChild(groupPosition, childPosition);

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.course_item_exlv, parent, false);
        }
        TextView name = (TextView) convertView.findViewById(R.id.elv_courseName);
        name.setText(course.title.toUpperCase());

        if (groupPosition == 0)
            name.setTextColor(Color.parseColor("#FFFF1A27"));
        else if (groupPosition == 1)
            name.setTextColor(Color.parseColor("#FF008A16"));
        else if (groupPosition == 2)
            name.setTextColor(Color.parseColor("#FF0004FF"));

        final MyDB db = new MyDB(convertView.getContext());
        ImageView delete = (ImageView) convertView.findViewById(R.id.elv_course_delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.deleteCourse(course.id);
                groupItems.get(groupname.get(groupPosition)).remove(childPosition);
                notifyDataSetChanged();
            }
        });

        final View finalConvertView = convertView;
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View dialogView = inflater.inflate(R.layout.view_course_info, null);
                TextView title = (TextView) dialogView.findViewById(R.id.dialogCourseTitle);
                title.setText(course.title);
                TextView start = (TextView) dialogView.findViewById(R.id.dialogCourseStart);
                start.setText(course.DtStart);
                TextView end = (TextView) dialogView.findViewById(R.id.dialogCourseEnd);
                end.setText(course.DtEnd);
                TextView time = (TextView) dialogView.findViewById(R.id.dialogCourseTime);
                time.setText(course.TStart + " - " + course.TEnd);
                TextView dow = (TextView) dialogView.findViewById(R.id.dialogCourseDow);
                String tempDow = "";
                for (int e = 0; e < course.dow.length; e++) {
                    if (course.dow[e].equals("1")) {
                        tempDow += getDow(e+1);

                    }
                }
                tempDow = tempDow.substring(0, tempDow.length() - 1);

                dow.setText(tempDow);
                Dialog info = new Dialog(mainView.getContext());
                info.setContentView(dialogView);
                info.setTitle("Course Details");
                info.show();
            }
        });
        return convertView;
    }

    public String getDow(int i) {
        String ret = "";
        if (i == 2)
            ret = "Mon,";
        else if (i == 3)
            ret = "Tue,";
        else if (i == 4)
            ret = "Wed,";
        else if (i == 5)
            ret = "Thu,";
        else if (i == 6)
            ret = "Fri,";
        else if (i == 7)
            ret = "Sat,";
        else if (i == 1)
            ret = "Sun,";
        return ret;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}